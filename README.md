<h1 align="center">Projeto 4</h1>
<h1 align="center">P2P - WebRTC</h1>

<h3 align="center"> UFG - INF </h3>
<h3 align="center"> Aplicações Distribuídas </h3>
<h3 align="center"> Bruno Tavares Pidde </h3>

<h3><b><i>Componentes do Projeto</i></b></h3>
<h3> - Arquitetura P2P </h3>
Arquitetura onde cada um das pontas ou dos nós pode funcionar como cliente ou como servidor.
O que fornece o compatilhameno de serviços, dados, arquivos sem precisar de um servidor central.

<h3> - WebRTC </h3>
WebRTC é uma API em desenvolvimento elaborada pela World Wide Web Consortium (W3C) para permitir aos navegadores executar aplicações de chamada telefônica, video chat e compartilhamento P2P sem a necessidade de plugins.

<h3> - JavaScript </h3>

<h3> - io.js </h3>


<h1> Tutorial </h1>

<h3>1 - Baixe o Projeto</h3>
---
<h3>2 - Baixe e instale o io.js clicando <a href="https://iojs.org/en/index.html"> aqui </a></h3>
---
<h3>3 - Antes de colocar o programa para funcionar, rode o comando <b>npm install</b> para instalar as dependências</h3> 
---
<h3>4 - Feito isso rode o comando npm start na pasta do projeto, como na imagem abaixo:</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/146/full/passo1.png?1499789299)
---
<h3>5 - Depois que o programa rodar, acesse a url <b> localhost:9966/#init </b> no Chrome</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/240/full/passo2.png?1499792407)
---
<h3>6 - Será gerado um código, esse código será seu ID, copie esse código, ele será muito importante</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/243/full/passo3.png?1499792518)
---
<h3>7 - Abra outra aba com a seguinte url <b>localhost:9966</br></h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/244/full/passo4.png?1499792601)
---
<h3>8 - Cole o ID que você copiou na área "ID amigo" e clique em Conectar</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/246/full/passo5.png?1499792747)
---
<h3>9 - Será gerado um ID na área "Seu ID", copie esse ID, cole na área "ID amigo" da primeira página de final "#init" e clique em conectar.</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/256/full/passo6.png?1499792896)
---
<h3>10 - A partir desse momento o Vídeo Chat terá início! :D</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/259/full/passo7.png?1499792999)
---
<h3>11 - Note que você também poderá se comunicar através de mensagens de texto</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/987/260/full/passo8.png?1499793056)


<h6 align="right">
Referência:
Foi utilizado os tutoriais do Kyle Robinsin Young
</h6>

